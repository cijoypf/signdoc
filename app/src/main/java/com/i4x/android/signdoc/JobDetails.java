package com.i4x.android.signdoc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;


public class JobDetails extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_job_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private TextView editTextCompanyName;
        private EditText editTextJobNumber;
        private EditText editTextDate;
        private EditText editTextSurname;
        private EditText editTextDevicePassword;
        private Button buttonNext;

        private String jobNumber, date, surname, devicePassword;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_job_details, container, false);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String companyName = prefs.getString(getString(R.string.key_company_name), getString(R.string.pref_default_company_name));

            editTextCompanyName = (TextView) rootView.findViewById(R.id.text_company_name_job_details);
            editTextCompanyName.setText(companyName);

            editTextJobNumber = (EditText) rootView.findViewById(R.id.edit_text_job_number);
            editTextDate = (EditText) rootView.findViewById(R.id.edit_text_date);
            editTextSurname = (EditText) rootView.findViewById(R.id.edit_text_surname);
            editTextDevicePassword = (EditText) rootView.findViewById(R.id.edit_text_device_password);

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String currentDate = sdf.format(new Date());
            editTextDate.setText(currentDate);

            buttonNext = (Button) rootView.findViewById(R.id.button_job_details_next);
            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jobNumber = editTextJobNumber.getText().toString();
                    date = editTextDate.getText().toString();
                    surname = editTextSurname.getText().toString();
                    devicePassword = editTextDevicePassword.getText().toString();

                    // Go to next page
                    Intent intent = new Intent(getActivity(), CaptureSignature.class);
                    intent.putExtra("jobNumber", jobNumber);
                    intent.putExtra("surname", surname);
                    intent.putExtra("date", date);
                    intent.putExtra("devicePassword", devicePassword);
                    startActivity(intent);
                }
            });

            return rootView;
        }
    }
}
