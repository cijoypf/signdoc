package com.i4x.android.signdoc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;


public class TermsAndConditions extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_terms_and_conditions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private CheckBox checkBox;
        private Button buttonNext;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);

            TextView textViewCompanyName = (TextView) rootView.findViewById(R.id.text_company_name_terms_and_conditions);
            TextView textViewDocumentName = (TextView) rootView.findViewById(R.id.text_document_name_terms_and_conditions);
            TextView textViewTermsAndConditions = (TextView) rootView.findViewById(R.id.text_terms_and_conditions);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

            String companyName = prefs.getString(getString(R.string.key_company_name), getString(R.string.pref_default_company_name));
            String documentName = prefs.getString(getString(R.string.key_document_name), getString(R.string.pref_default_document_name));
            String phone = prefs.getString(getString(R.string.key_phone), getString(R.string.pref_default_phone));
            String email = prefs.getString(getString(R.string.key_email), getString(R.string.pref_default_email));

            textViewCompanyName.setText(companyName);
            textViewDocumentName.setText(documentName);

            String para1Heading = prefs.getString(getString(R.string.key_para1_heading), getString(R.string.pref_default_para1_heading));
            String para1Contents = prefs.getString(getString(R.string.key_para1_contents), getString(R.string.pref_default_para1_contents));
            String para2Heading = prefs.getString(getString(R.string.key_para2_heading), getString(R.string.pref_default_para2_heading));
            String para2Contents = prefs.getString(getString(R.string.key_para2_contents), getString(R.string.pref_default_para2_contents));
            String para3Heading = prefs.getString(getString(R.string.key_para3_heading), getString(R.string.pref_default_para3_heading));
            String para3Contents = prefs.getString(getString(R.string.key_para3_contents), getString(R.string.pref_default_para3_contents));
            String para4Heading = prefs.getString(getString(R.string.key_para4_heading), getString(R.string.pref_default_para4_heading));
            String para4Contents = prefs.getString(getString(R.string.key_para4_contents), getString(R.string.pref_default_para4_contents));
            String para5Heading = prefs.getString(getString(R.string.key_para5_heading), getString(R.string.pref_default_para5_heading));
            String para5Contents = prefs.getString(getString(R.string.key_para5_contents), getString(R.string.pref_default_para5_contents));
            String para6Heading = prefs.getString(getString(R.string.key_para6_heading), getString(R.string.pref_default_para6_heading));
            String para6Contents = prefs.getString(getString(R.string.key_para6_contents), getString(R.string.pref_default_para6_contents));


            String termsAndConditions = "<html><p><br>Dear Consumer,<br><br>"
                    + "<b>" + para1Heading + "</b><br>"
                    + para1Contents + "<br><br>"
                    + "<b>" + para2Heading + "</b><br>"
                    + para2Contents + "<br><br>"
                    + "<b>" + para3Heading + "</b><br>"
                    + para3Contents + "<br><br>"
                    + "<b>" + para4Heading + "</b><br>"
                    + para4Contents + "<br><br>"
                    + "<b>" + para5Heading + "</b><br>"
                    + para5Contents + "<br><br>"
                    + "<b>" + para6Heading + "</b><br>"
                    + para6Contents + "<br><br>"
                    + "Thank you<br>"
                    + companyName + "<br>"
                    + "Phone: " + phone + "<br>"
                    + "Email: "+ email +  "<br><br></p></html>";
            textViewTermsAndConditions.setText(Html.fromHtml(termsAndConditions));

            buttonNext = (Button) rootView.findViewById(R.id.button_next_terms_and_conditions);
            buttonNext.setEnabled(false);

            checkBox = (CheckBox) rootView.findViewById(R.id.checkbox_terms_and_conditions);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkBox.isChecked()) {
                        buttonNext.setEnabled(true);
                    } else {
                        buttonNext.setEnabled(false);
                    }
                }
            });

            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), JobDetails.class);
                    startActivity(intent);
                }
            });

            return rootView;
        }
    }
}
