package com.i4x.android.signdoc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import java.io.File;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class Alarm extends BroadcastReceiver {
	private final String REMINDER_BUNDLE = "ReminderBundle";
	private final String TAG = "Alarm";

	private SharedPreferences SignDocPreferences;
	private SharedPreferences.Editor SignDocPreferencesEditor;
	private final String PREFS_NAME = "SignDocPreferences";
	private final String ALARM = "alarm";
	private final String SIGNATURE_DIRECTORY = "signatueDirectory";
	private final String FTP_FILE_INDEX = "ftpFileIndex";

	private int ftpFileIndex;
	FtpUpload ftpUpload;
	private String signatureDirectory;
	private String jobNumber;

	private Handler handler;
	private Runnable runnable;
	private int count;
	private Context context;

	private String ftpStatus;
	private LogWriter logWriter;

	public Alarm() {
	}

	public Alarm(Context context, Bundle extras, int timeoutInSeconds) {
		this.context = context;

		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, Alarm.class);
		intent.putExtra(REMINDER_BUNDLE, extras);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		Calendar time = Calendar.getInstance();
		time.setTimeInMillis(System.currentTimeMillis());
		time.add(Calendar.SECOND, timeoutInSeconds);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeoutInSeconds,
				timeoutInSeconds, pendingIntent);
	}

	@Override
	public void onReceive(final Context context, Intent intent) {
		
		this.context = context;
		logWriter = new LogWriter();

        SignDocPreferences = context.getSharedPreferences(
				PREFS_NAME, 0);
        SignDocPreferencesEditor = SignDocPreferences
				.edit();

		signatureDirectory = SignDocPreferences.getString(
				SIGNATURE_DIRECTORY, "");

		if (SignDocPreferences.getBoolean(ALARM, false)) {
			logWriter.logDebug(TAG, "15 mins Alarm went off");

			// Resend FTP files here.
			// Iterate files from Preferences.
			ftpFileIndex = SignDocPreferences.getInt(
					FTP_FILE_INDEX, 0);
			if (ftpFileIndex > 0) {

				handler = new Handler();
				runnable = new Runnable() {
					@Override
					public void run() {
						logWriter.logDebug(TAG,
								"Inside run method. Testing Handler 30 seconds");
						ftpFileIndex = SignDocPreferences
								.getInt(FTP_FILE_INDEX, 0);
						jobNumber = SignDocPreferences
								.getString(Integer.toString(ftpFileIndex), "NA");

						try {
							ftpUpload = new FtpUpload(context, "resend");
							ftpStatus = ftpUpload.execute(
									new File(signatureDirectory + jobNumber))
									.get();
							logWriter.logDebug(TAG, "################## FTP Status: "
									+ ftpStatus);
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						}

						if (ftpStatus.equals("FileTransferCompleted")) {

							// Delete ftpFileIndex and file name from preference
                            SignDocPreferencesEditor
									.remove(Integer.toString(ftpFileIndex));
                            SignDocPreferencesEditor.commit();
							logWriter.logDebug(TAG,
									"Successfully resent File. ftpFileIndex: "
											+ ftpFileIndex + ", jobNumber: "
											+ jobNumber);
							ftpFileIndex--;
                            SignDocPreferencesEditor.putInt(
									FTP_FILE_INDEX, ftpFileIndex);
                            SignDocPreferencesEditor.commit();
						} else {
							logWriter.logDebug(TAG,
									"File Transfer not completed, not deleting file: "
											+ jobNumber + " from preferences");
						}

						if (ftpFileIndex > 0
								&& ftpStatus.equals("FileTransferCompleted")) {
							handler.postDelayed(runnable, 30000);
						}
					}
				};
				handler.postDelayed(runnable, 5000);

			}

		} else {
            SignDocPreferencesEditor.putBoolean(ALARM, true);
            SignDocPreferencesEditor.commit();
			logWriter.logDebug(TAG, "Alarm Manager initial run");
		}

		// Remove AlarmManager if ftpFileIndex = 0
		ftpFileIndex = SignDocPreferences.getInt(FTP_FILE_INDEX,
				0);
		if (ftpFileIndex <= 0) {
			Intent alarmIntent = new Intent(context, Alarm.class);
			Bundle extras = new Bundle();
			alarmIntent.putExtra(REMINDER_BUNDLE, extras);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
			logWriter.logDebug(TAG, "Removed Alarm as ftpFileIndex = 0");
		}


	}

}
