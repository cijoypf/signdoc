package com.i4x.android.signdoc;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pdfjet.A4;
import com.pdfjet.Align;
import com.pdfjet.Box;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Paragraph;
import com.pdfjet.TextBox;
import com.pdfjet.TextColumn;
import com.pdfjet.TextLine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;


public class CaptureSignature extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_signature);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_capture_signature, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private String jobNumber, surname, date, devicePassword;

        private String signatureDir;
        private String uniqueId;
        private String current = null;
        private File mypath;
        private LinearLayout mContent;
        private signature mSignature;
        private Bitmap mBitmap;

        private Button buttonEmail, buttonClear;
        private View mView;

        private LogWriter logWriter;
        private EditText input;
        private InputMethodManager imm;

        private String customerEmail;
        private String documentName;
        private String companyName;
        private String companyEmail;
        private String greeting;
        private String companyPhone;
        private String para1Heading;
        private String para1Contents;
        private String para2Heading;
        private String para2Contents;
        private String para3Heading;
        private String para3Contents;
        private String para4Heading;
        private String para4Contents;
        private String para5Heading;
        private String para5Contents;
        private String para6Heading;
        private String para6Contents;

        private AlertDialog alertDialog;

        private SharedPreferences SignDocPreferences;
        private SharedPreferences.Editor SignDocPreferencesEditor;
        private final String PREFS_NAME = "SignDocPreferences";
        private final String SIGNATURE_DIRECTORY = "signatueDirectory";

        private final String TAG = "CaptureSignature";


        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_capture_signature, container, false);

            logWriter = new LogWriter();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            documentName = prefs.getString(getString(R.string.key_document_name), getString(R.string.pref_default_document_name));
            companyName = prefs.getString(getString(R.string.key_company_name), getString(R.string.pref_default_company_name));
            companyEmail = prefs.getString(getString(R.string.key_email), getString(R.string.pref_default_email));
            greeting = prefs.getString(getString(R.string.key_greeting), getString(R.string.pref_default_greeting));
            companyPhone = prefs.getString(getString(R.string.key_phone), getString(R.string.pref_default_phone));

            para1Heading = prefs.getString(getString(R.string.key_para1_heading), getString(R.string.pref_default_para1_heading));
            para1Contents = prefs.getString(getString(R.string.key_para1_contents), getString(R.string.pref_default_para1_contents));
            para2Heading = prefs.getString(getString(R.string.key_para2_heading), getString(R.string.pref_default_para2_heading));
            para2Contents = prefs.getString(getString(R.string.key_para2_contents), getString(R.string.pref_default_para2_contents));
            para3Heading = prefs.getString(getString(R.string.key_para3_heading), getString(R.string.pref_default_para3_heading));
            para3Contents = prefs.getString(getString(R.string.key_para3_contents), getString(R.string.pref_default_para3_contents));
            para4Heading = prefs.getString(getString(R.string.key_para4_heading), getString(R.string.pref_default_para4_heading));
            para4Contents = prefs.getString(getString(R.string.key_para4_contents), getString(R.string.pref_default_para4_contents));
            para5Heading = prefs.getString(getString(R.string.key_para5_heading), getString(R.string.pref_default_para5_heading));
            para5Contents = prefs.getString(getString(R.string.key_para5_contents), getString(R.string.pref_default_para5_contents));
            para6Heading = prefs.getString(getString(R.string.key_para6_heading), getString(R.string.pref_default_para6_heading));
            para6Contents = prefs.getString(getString(R.string.key_para6_contents), getString(R.string.pref_default_para6_contents));

            Bundle extras = getActivity().getIntent().getExtras();
            jobNumber = extras.getString("jobNumber");
            surname = extras.getString("surname");
            devicePassword = extras.getString("devicePassword");
            date = extras.getString("date");
            logWriter.logDebug(TAG, "jobNumber:" + jobNumber);
            logWriter.logDebug(TAG, "surname:" + surname);
            logWriter.logDebug(TAG, "devicePassword:" + devicePassword);
            logWriter.logDebug(TAG, "date:" + date);

            signatureDir = Environment.getExternalStorageDirectory() + "/"
                    + getResources().getString(R.string.app_name) + "/";

            File file = new File(signatureDir);
            logWriter.logDebug(TAG, "Signature Directory: " + signatureDir);
            prepareDirectory();

            SignDocPreferences = getActivity().getSharedPreferences(
                    PREFS_NAME, 0);
            SignDocPreferencesEditor = SignDocPreferences
                    .edit();

            SignDocPreferencesEditor.putString(SIGNATURE_DIRECTORY,
                    signatureDir);
            SignDocPreferencesEditor.commit();

            // Signature capture
            uniqueId = "1";
            current = uniqueId + ".jpeg";
            logWriter.logDebug(TAG, "File Name: " + current);
            mypath = new File(file, current);

            mContent = (LinearLayout) rootView.findViewById(R.id.signature_area_capture_signature);
            buttonEmail = (Button) rootView.findViewById(R.id.button_email_capture_signature);
            buttonClear = (Button) rootView.findViewById(R.id.button_clear_capture_signature);
            buttonEmail.setEnabled(false);

            mSignature = new signature(getActivity(), null);
            mSignature.setBackgroundColor(android.graphics.Color.WHITE);
            mContent.addView(mSignature, ActionBar.LayoutParams.FILL_PARENT,
                    ActionBar.LayoutParams.FILL_PARENT);
            mView = mContent;

            buttonClear.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    logWriter.logInfo("log_tag", "Panel Cleared");
                    mSignature.clear();
                    buttonEmail.setEnabled(false);
                }
            });

            buttonEmail.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    logWriter.logInfo(TAG, "Email Clicked");

                    mView.setDrawingCacheEnabled(true);
                    mSignature.save(mView);
                    Bundle b = new Bundle();
                    b.putString("status", "done");
                    Intent intent = new Intent();
                    intent.putExtras(b);
                    getActivity().setResult(RESULT_OK, intent);

                    createEmailDialog();

                }

                private void createEmailDialog() {

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Please Enter Customer Email:");

                    // Set an EditText view to get user input
                    input = new EditText(getActivity());
                    input.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                    imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    alert.setView(input);

                    alert.setPositiveButton("Send Email",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {

                                    customerEmail = input.getEditableText()
                                            .toString();

                                    // Send Email
                                    sendEmail(customerEmail);

                                    // FTP Upload
                                    FtpUpload ftpUpload = new FtpUpload(getActivity(),
                                            "firstTimeSend");
                                    ftpUpload.execute(new File(signatureDir
                                            + jobNumber + ".pdf"));
//                                    imm.hideSoftInputFromWindow(
//                                            input.getWindowToken(), 0);

                                }

                            });
                    alert.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {

                                    dialog.cancel();
                                }
                            });
                    alertDialog = alert.create();
                    alertDialog.show();

                }

            });

            return rootView;
        }

        private void sendEmail(String customerEmail) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{customerEmail});

            if (!companyEmail.equals("")) {
                sendIntent.putExtra(Intent.EXTRA_BCC,
                        new String[]{companyEmail});
            }

            sendIntent.putExtra(Intent.EXTRA_SUBJECT,
                    documentName + " - Job Number: " + jobNumber);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    documentName + " - Job Number: " + jobNumber);
            sendIntent.putExtra(Intent.EXTRA_STREAM,
                    Uri.fromFile(new File(signatureDir + jobNumber + ".pdf")));
            sendIntent.setType("vnd.android.cursor.dir/email");
            startActivityForResult(Intent.createChooser(sendIntent, "Email:"), 1);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            logWriter.logDebug(TAG, "requestCode: " + requestCode);
            logWriter.logDebug(TAG, "resultCode: " + resultCode);
            if (requestCode == 1) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        }

        private void prepareDirectory() {
            try {
                makedirs();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(),
                        "Could not initiate File System.. Is SD card mounted properly?",
                        Toast.LENGTH_LONG).show();
            }
        }

        private void makedirs() {
            File tempdir = new File(signatureDir);
            if (!tempdir.exists()) {
                tempdir.mkdirs();
            }

        }


        public class signature extends View {
            private static final float STROKE_WIDTH = 5f;
            private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
            private Paint paint = new Paint();
            private Path path = new Path();

            private float lastTouchX;
            private float lastTouchY;
            private final RectF dirtyRect = new RectF();

            public signature(Context context, AttributeSet attrs) {
                super(context, attrs);
                paint.setAntiAlias(true);
                paint.setColor(android.graphics.Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(STROKE_WIDTH);
            }

            public void save(View v) {
                logWriter.logInfo("log_tag", "Width: " + v.getWidth());
                logWriter.logInfo("log_tag", "Height: " + v.getHeight());
                if (mBitmap == null) {
                    mBitmap = Bitmap.createBitmap(mContent.getWidth(),
                            mContent.getHeight(), Bitmap.Config.RGB_565);
                }

                Canvas canvas = new Canvas(mBitmap);
                try {
                    FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                    v.draw(canvas);
                    mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mFileOutStream);
                    mFileOutStream.flush();
                    mFileOutStream.close();
                    String url = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                            mBitmap, "title", null);
                    logWriter.logInfo("log_tag", "url: " + url);
                    // To delete the file
                    // boolean deleted = mypath.delete();
                    // Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                    // If you want to convert the image to string use base64
                    // converter

                    // Read pdf

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(signatureDir + jobNumber
                                + ".pdf");
                        PDF pdf = new PDF(fos);
                        pdf.setTitle(companyName + " - " + documentName);
                        pdf.setSubject(documentName);
                        pdf.setAuthor(companyName);
                        Page page = new Page(pdf, A4.PORTRAIT);

                        Font titleFont = new Font(pdf, CoreFont.HELVETICA);
                        Font bodyFont = new Font(pdf, CoreFont.HELVETICA);
                        Font headingFont = new Font(pdf, CoreFont.HELVETICA_BOLD);
                        titleFont.setSize(18);
                        bodyFont.setSize(10);
                        headingFont.setSize(12);

                        TextColumn column = new TextColumn();
                        // column.setLineBetweenParagraphs(true);
                        column.setLineSpacing(1.5);

                        Paragraph pCompanyNameTitle = new Paragraph();
                        pCompanyNameTitle.setAlignment(Align.CENTER);
                        pCompanyNameTitle.add(new TextLine(titleFont,
                                companyName + " Pty Ltd"));

                        Paragraph pDocumentName = new Paragraph();
                        pDocumentName.setAlignment(Align.CENTER);
                        pDocumentName.add(new TextLine(titleFont, documentName));

                        Paragraph newLineParagraph = new Paragraph();
                        newLineParagraph.setAlignment(Align.JUSTIFY);
                        newLineParagraph.add(new TextLine(titleFont, ""));

                        Paragraph pGreeting = new Paragraph();
                        pGreeting.setAlignment(Align.JUSTIFY);
                        pGreeting.add(new TextLine(bodyFont, greeting));

                        Paragraph p1Heading = new Paragraph();
                        p1Heading.setAlignment(Align.JUSTIFY);
                        p1Heading.add(new TextLine(headingFont, para1Heading));

                        Paragraph p1Contents = new Paragraph();
                        p1Contents.setAlignment(Align.JUSTIFY);
                        p1Contents.add(new TextLine(bodyFont, para1Contents));

                        Paragraph p2Heading = new Paragraph();
                        p2Heading.setAlignment(Align.JUSTIFY);
                        p2Heading.add(new TextLine(headingFont, para2Heading));

                        Paragraph p2Contents = new Paragraph();
                        p2Contents.setAlignment(Align.JUSTIFY);
                        p2Contents.add(new TextLine(bodyFont, para2Contents));

                        Paragraph p3Heading = new Paragraph();
                        p3Heading.setAlignment(Align.JUSTIFY);
                        p3Heading.add(new TextLine(headingFont, para3Heading));

                        Paragraph p3Contents = new Paragraph();
                        p3Contents.setAlignment(Align.JUSTIFY);
                        p3Contents.add(new TextLine(bodyFont, para3Contents));

                        Paragraph p4Heading = new Paragraph();
                        p4Heading.setAlignment(Align.JUSTIFY);
                        p4Heading.add(new TextLine(headingFont, para4Heading));

                        Paragraph p4Contents = new Paragraph();
                        p4Contents.setAlignment(Align.JUSTIFY);
                        p4Contents.add(new TextLine(bodyFont, para4Contents));

                        Paragraph p5Heading = new Paragraph();
                        p5Heading.setAlignment(Align.JUSTIFY);
                        p5Heading.add(new TextLine(headingFont, para5Heading));

                        Paragraph p5Contents = new Paragraph();
                        p5Contents.setAlignment(Align.JUSTIFY);
                        p5Contents.add(new TextLine(bodyFont, para5Contents));

                        Paragraph p6Heading = new Paragraph();
                        p6Heading.setAlignment(Align.JUSTIFY);
                        p6Heading.add(new TextLine(headingFont, para6Heading));

                        Paragraph p6Contents = new Paragraph();
                        p6Contents.setAlignment(Align.JUSTIFY);
                        p6Contents.add(new TextLine(headingFont, para6Contents));

                        Paragraph pThankYou = new Paragraph();
                        pThankYou.setAlignment(Align.JUSTIFY);
                        pThankYou.add(new TextLine(bodyFont, "Thank you"));

                        Paragraph pCompanyName = new Paragraph();
                        pCompanyName.setAlignment(Align.JUSTIFY);
                        pCompanyName.add(new TextLine(bodyFont, companyName));

                        Paragraph pPhone = new Paragraph();
                        pPhone.setAlignment(Align.JUSTIFY);
                        pPhone.add(new TextLine(bodyFont, companyPhone));

                        Paragraph pEmail = new Paragraph();
                        pEmail.setAlignment(Align.JUSTIFY);
                        pEmail.add(new TextLine(bodyFont, companyEmail));

                        Paragraph pJobNumber = new Paragraph();
                        pJobNumber.setAlignment(Align.JUSTIFY);
                        pJobNumber.add(new TextLine(bodyFont, "Job Number: " + jobNumber));

                        Paragraph pDate = new Paragraph();
                        pDate.setAlignment(Align.JUSTIFY);
                        pDate.add(new TextLine(bodyFont, "Date: " + date));

                        Paragraph pSurname = new Paragraph();
                        pSurname.setAlignment(Align.JUSTIFY);
                        pSurname.add(new TextLine(
                                bodyFont, "Surname: " + surname));

                        Paragraph pDevicePassword = new Paragraph();
                        pDevicePassword.setAlignment(Align.JUSTIFY);
                        pDevicePassword.add(new TextLine(bodyFont,
                                "Device Password if applicable:  " + devicePassword));

                        column.addParagraph(newLineParagraph);
                        column.addParagraph(pCompanyNameTitle);
                        column.addParagraph(pDocumentName);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(pGreeting);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(p1Heading);
                        column.addParagraph(p1Contents);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(p2Heading);
                        column.addParagraph(p2Contents);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(p3Heading);
                        column.addParagraph(p3Contents);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(p4Heading);
                        column.addParagraph(p4Contents);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(p5Heading);
                        column.addParagraph(p5Contents);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(p6Heading);
                        column.addParagraph(p6Contents);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(pThankYou);
                        column.addParagraph(pCompanyName);
                        column.addParagraph(pPhone);
                        column.addParagraph(pEmail);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(pJobNumber);
                        column.addParagraph(pDate);
                        column.addParagraph(pSurname);
                        column.addParagraph(newLineParagraph);
                        column.addParagraph(pDevicePassword);

                        column.setPosition(85, 80);
                        column.setSize(420, 400);
                        column.drawOn(page);

                        TextBox textBox = new TextBox(bodyFont, "Customer Signature", 100, 15);
                        textBox.setPosition(365, 590);
                        textBox.setNoBorders();
                        textBox.drawOn(page);

                        Box box = new Box(365, 620, 150, 150);


                        File file = new File(signatureDir + "1.jpeg");
                        InputStream f = new FileInputStream(file);
                        Image image = new Image(pdf, f, ImageType.JPG);
                        //image.setPosition(85, 602);
                        image.placeIn(box);
                        image.scaleBy(0.11);

                        //box.drawOn(page);
                        image.drawOn(page);

                        pdf.flush();
                        fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    logWriter.logDebug("log_tag", e.toString());
                }
            }

            public void clear() {
                path.reset();
                invalidate();
            }

            @Override
            protected void onDraw(Canvas canvas) {
                canvas.drawPath(path, paint);
            }

            @Override
            public boolean onTouchEvent(MotionEvent event) {
                float eventX = event.getX();
                float eventY = event.getY();
                buttonEmail.setEnabled(true);

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        path.moveTo(eventX, eventY);
                        lastTouchX = eventX;
                        lastTouchY = eventY;
                        return true;

                    case MotionEvent.ACTION_MOVE:

                    case MotionEvent.ACTION_UP:

                        resetDirtyRect(eventX, eventY);
                        int historySize = event.getHistorySize();
                        for (int i = 0; i < historySize; i++) {
                            float historicalX = event.getHistoricalX(i);
                            float historicalY = event.getHistoricalY(i);
                            expandDirtyRect(historicalX, historicalY);
                            path.lineTo(historicalX, historicalY);
                        }
                        path.lineTo(eventX, eventY);
                        break;

                    default:
                        debug("Ignored touch event: " + event.toString());
                        return false;
                }

                invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

                lastTouchX = eventX;
                lastTouchY = eventY;

                return true;
            }

            private void debug(String string) {
            }

            private void expandDirtyRect(float historicalX, float historicalY) {
                if (historicalX < dirtyRect.left) {
                    dirtyRect.left = historicalX;
                } else if (historicalX > dirtyRect.right) {
                    dirtyRect.right = historicalX;
                }

                if (historicalY < dirtyRect.top) {
                    dirtyRect.top = historicalY;
                } else if (historicalY > dirtyRect.bottom) {
                    dirtyRect.bottom = historicalY;
                }
            }

            private void resetDirtyRect(float eventX, float eventY) {
                dirtyRect.left = Math.min(lastTouchX, eventX);
                dirtyRect.right = Math.max(lastTouchX, eventX);
                dirtyRect.top = Math.min(lastTouchY, eventY);
                dirtyRect.bottom = Math.max(lastTouchY, eventY);

            }
        }

    }
}
