package com.i4x.android.signdoc;

import android.util.Log;

public class LogWriter {
	
	private final boolean loggingEnabled = false;
	
	public void logError(String TAG, String log) {	
		if(loggingEnabled) {
			Log.e(TAG, log);
		}
	}
	
	public void logWarning(String TAG, String log) {	
		if(loggingEnabled) {
			Log.w(TAG, log);
		}
	}
	
	public void logDebug(String TAG, String log) {	
		if(loggingEnabled) {
			Log.d(TAG, log);
		}
	}
	
	public void logInfo(String TAG, String log) {	
		if(loggingEnabled) {
			Log.i(TAG, log);
		}
	}

}
