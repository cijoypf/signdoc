package com.i4x.android.signdoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;

import java.io.File;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

public class FtpUpload extends AsyncTask<File, Void, String> {

	private SharedPreferences SignDocPreferences;
	private SharedPreferences.Editor SignDocPreferencesEditor;
	private final String PREFS_NAME = "SignDocPreferences";
	private final String FTP_FILE_INDEX = "ftpFileIndex";
	private final String ALARM = "alarm";
	private File mFile;
	private String ftpStatus = "NA";
	private String ftpRequestType = "";

	private int ftpFileIndex;
	private LogWriter logWriter;
	private String FTP_HOST; // = "66.220.9.50";
    private int FTP_PORT;// = 21;
	private String FTP_USER;// = "signdoc";
	private String FTP_PASS;// = "SignDoc2015";
	
	private final String TAG = "FtpUpload";
	private Context context;

	public FtpUpload(Context context, String ftpRequestType) {
		this.context = context;
		this.ftpRequestType = ftpRequestType;
		logWriter = new LogWriter();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        FTP_HOST = prefs.getString(context.getString(R.string.key_ftp_server_ip), context.getString(R.string.pref_default_ftp_server_ip));
        FTP_PORT = Integer.parseInt(prefs.getString(context.getString(R.string.key_ftp_server_port), context.getString(R.string.pref_default_ftp_server_port)));
        FTP_USER = prefs.getString(context.getString(R.string.key_ftp_server_username), context.getString(R.string.pref_default_ftp_server_username));
        FTP_PASS = prefs.getString(context.getString(R.string.key_ftp_server_password), context.getString(R.string.pref_default_ftp_server_password));
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(File... file) {

        SignDocPreferences = context.getSharedPreferences(
				PREFS_NAME, 0);
        SignDocPreferencesEditor = SignDocPreferences
				.edit();
		System.setProperty("ftp4j.activeDataTransfer.acceptTimeout", "60000");

		mFile = file[0];
		logWriter.logDebug(TAG, "File Name from FTPUpload Class: " + mFile.getName());

		FTPClient client = new FTPClient();

		try {
			client.connect(FTP_HOST, FTP_PORT);
			client.login(FTP_USER, FTP_PASS);
			client.setType(FTPClient.TYPE_BINARY);
            //client.changeDirectory("/SignDoc/");
			client.upload(file[0], new MyFtpTransferListener());

		} catch (Exception e) {

			// Connection Error, save file name to preferences to send later.
			logWriter.logError(TAG, "Connection Error - " + e.toString());
			ftpStatus = "Connection Error";
			addFileNameToPrefsToSendLater();
			try {
				client.disconnect(true);
			} catch (Exception e2) {
				logWriter.logError(TAG, "Error while disconnecting FTP Client");
//				e2.printStackTrace();
			}
		}

		return ftpStatus;

	}

	/******* Used to file upload and show progress **********/

	public class MyFtpTransferListener implements FTPDataTransferListener {

		public void started() {
			// Transfer started
			logWriter.logDebug(TAG, "UploadStarted");
			ftpStatus = "UploadStarted";
		}

		public void transferred(int length) {
			// Yet other length bytes has been transferred since the last time
			// this
			// method was called
			logWriter.logDebug(TAG, "FileTransferred" + length);
			ftpStatus = "FileTransferred:" + length;
		}

		public void completed() {
			// Transfer completed
			logWriter.logDebug(TAG, "FileTransferCompleted");
			ftpStatus = "FileTransferCompleted";
		}

		public void aborted() {
			// Transfer aborted
			logWriter.logError(TAG, "FileTransferAborted");
			ftpStatus = "FileTransferAborted";
			addFileNameToPrefsToSendLater();
		}

		public void failed() {
			// Transfer failed
			logWriter.logError(TAG, "FileTransferFailed");
			ftpStatus = "FileTransferFailed";
			addFileNameToPrefsToSendLater();
		}

	}

	public void addFileNameToPrefsToSendLater() {

		if (ftpRequestType.equals("firstTimeSend")) {

			ftpFileIndex = SignDocPreferences.getInt(
					FTP_FILE_INDEX, 0);
			ftpFileIndex++;
            SignDocPreferencesEditor.putInt(FTP_FILE_INDEX,
					ftpFileIndex);
            SignDocPreferencesEditor.commit();

            SignDocPreferencesEditor.putString(
					Integer.toString(ftpFileIndex), mFile.getName());
            SignDocPreferencesEditor.commit();
			logWriter.logDebug(TAG,
					"File name saved to preferences to send later. ftpFileIndex: "
							+ ftpFileIndex + ", FileName: " + mFile.getName());

            SignDocPreferencesEditor.putBoolean(ALARM, false);
            SignDocPreferencesEditor.commit();

			if (ftpFileIndex > 0) {
				Bundle bundle = new Bundle();
				new Alarm(context, bundle, 15 * 60 * 1000);
				logWriter.logDebug(TAG, "Alarm Set for 15 mins");
			}
		} else {
			logWriter.logDebug(TAG,
					"addFileNameToPrefsToSendLater() - this method is not executed as the call was resend from AlarmManager");
		}

	}
}
